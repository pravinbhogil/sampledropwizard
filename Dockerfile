FROM openjdk:8-jdk-alpine
ADD target/SampleDropwizard.war SampleDropwizard.war
EXPOSE 8080
ENTRYPOINT [ "mvn", "compile", "exec:java"]