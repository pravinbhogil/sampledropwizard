package com.dropwizard;

import javax.validation.Valid;

import io.dropwizard.Configuration;

public class ExampleServiceConfiguration extends Configuration {

	  @Valid
	    private MessagesConfiguration messages;

	    public MessagesConfiguration getMessages() {
	        return messages;
	    }

	    public void setMessages(MessagesConfiguration messages) {
	        this.messages = messages;
	    }
}
