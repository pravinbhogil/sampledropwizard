package com.dropwizard;

import com.dropwizard.controller.HelloResource;

import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

public class BlogService extends Application<ExampleServiceConfiguration>{

	  public static void main(String[] args) throws Exception {
	        new BlogService().run(args);
	    }
	
	
	@Override
	public void run(ExampleServiceConfiguration conf, Environment environment) throws Exception {
		 //Injector injector = Guice.createInjector();
		// environment.jersey().register(injector.getInstance(HelloResource.class));
		 environment.jersey().register(new HelloResource(conf.getMessages()));
		
	}
	
	  @Override
	 public void initialize(Bootstrap<ExampleServiceConfiguration> bootstrap) {
	 }

}
