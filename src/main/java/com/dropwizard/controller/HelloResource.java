package com.dropwizard.controller;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.dropwizard.MessagesConfiguration;
@Produces(MediaType.APPLICATION_JSON)
@Path(value = "/hello")
public class HelloResource {
	  
    private final MessagesConfiguration conf;

  
    public HelloResource(MessagesConfiguration conf) {
        this.conf = conf;
    }

    @GET
    public String sayHello() {
        return conf.getHello();
    }

}