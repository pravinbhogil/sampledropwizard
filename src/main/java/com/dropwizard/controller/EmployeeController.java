package com.dropwizard.controller;

import java.util.Arrays;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.dropwizard.entity.Blog;

@Produces(MediaType.APPLICATION_JSON)
public class EmployeeController {

	@GET
	  public List<Blog> index() {
	        return Arrays.asList(new Blog("Day 12: OpenCV--Face Detection for Java Developers",
	                "https://www.openshift.com/blogs/day-12-opencv-face-detection-for-java-developers"));
	    }
}
